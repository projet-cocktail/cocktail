﻿global using CocktailClient.Client.Pages;
global using Microsoft.AspNetCore.Components;
global using System.Net.Http.Json;

namespace CocktailClient.Client.Services.SuperRecetteService
{
    public class SuperRecetteService : ISuperRecetteService
    {
        private readonly HttpClient _http;

        private readonly NavigationManager _navigationManager;

        public SuperRecetteService(HttpClient http, NavigationManager navigationManager)
        {
            _http = http;
            _navigationManager = navigationManager;
        }

        public List<CocktailClient.Shared.SuperRecette> Recettes { get; set; } = new List<CocktailClient.Shared.SuperRecette>();
        public List<Ingredient> Ingredients { get; set; } = new List<Ingredient>();

        public async Task CreateRecette(CocktailClient.Shared.SuperRecette recette)
        {
            var result = await _http.PostAsJsonAsync("api/superrecette", recette);
            await SetRecettes(result);
        }

        private async Task SetRecettes(HttpResponseMessage result)
        {
            var response = await result.Content.ReadFromJsonAsync<List<CocktailClient.Shared.SuperRecette>>();
            Recettes = response;
            _navigationManager.NavigateTo("superrecettes");
        }

        public async Task DeleteRecette(int id)
        {
            var result = await _http.DeleteAsync($"api/superrecette/{id}");
            await SetRecettes(result);
        }

        public async Task GetIngredients()
        {
            var result = await _http.GetFromJsonAsync<List<Ingredient>>("api/superrecette/ingredients");
            if (result != null)
                Ingredients = result;
        }

        public async Task<CocktailClient.Shared.SuperRecette> GetSingleRecette(int id)
        {
            var result = await _http.GetFromJsonAsync<CocktailClient.Shared.SuperRecette>($"api/superrecette/{id}");
            if (result != null)
                return result;
            throw new Exception("Recette not found!");
        }

        public async Task GetSuperRecettes()
        {
            var result = await _http.GetFromJsonAsync<List<CocktailClient.Shared.SuperRecette>>("api/superrecette");
            if (result != null)
                Recettes = result;
        }

        public async Task UpdateRecette(CocktailClient.Shared.SuperRecette recette)
        {
            var result = await _http.PutAsJsonAsync($"api/superrecette/{recette.Id}", recette);
            await SetRecettes(result);
        }
    }
}
