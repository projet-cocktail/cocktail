﻿namespace CocktailClient.Client.Services.SuperRecetteService
{
    public interface ISuperRecetteService
    {
        List<CocktailClient.Shared.SuperRecette> Recettes { get; set; }
        List<Ingredient> Ingredients { get; set; }
        Task GetIngredients();
        Task GetSuperRecettes();
        Task<CocktailClient.Shared.SuperRecette> GetSingleRecette(int id);
        Task CreateRecette(CocktailClient.Shared.SuperRecette recette);
        Task UpdateRecette(CocktailClient.Shared.SuperRecette recette);
        Task DeleteRecette(int id);
    }
}
