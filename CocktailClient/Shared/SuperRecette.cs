﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailClient.Shared
{
    public class SuperRecette
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public List<Ingredient>? Ingredients { get; set; }
        public string IngredientName { get; set; } = string.Empty.ToString();
        public int IngredientId { get; set; }
    }
}
