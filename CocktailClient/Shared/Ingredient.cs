﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailClient.Shared
{
    public class Ingredient
    {
        public int Id { get; set; }
        
        public string IngredientName { get; set; } = string.Empty;       

        public List<SuperRecette> Recettes { get; set; } = new List<SuperRecette>();

    }
}
