﻿using CocktailClient.Shared;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace CocktailClient.Server.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ingredient>().HasData(
                new Ingredient { Id = 1, IngredientName = "rhum" },
                new Ingredient { Id = 2, IngredientName = "grenadine" },
                new Ingredient { Id = 3, IngredientName = "whisky" },
                new Ingredient { Id = 4, IngredientName = "glace" }
            );

            modelBuilder.Entity<SuperRecette>().HasData(
                new SuperRecette
                {
                    Id = 1,
                    Name = "ABC",
                    IngredientName = "amaretto, cognac, bailey",
                    Description = "boisson forte"
                },
                new SuperRecette
                {
                    Id = 2,
                    Name = "Grenadine", 
                    IngredientName = "sirop de grenadine, eau",
                    Description = "à consommer à tout heure"
                },
                new SuperRecette
                {
                    Id = 3,
                    Name = "Mojito",
                    IngredientName = "rhum, sucre de canne, eau gazeuse, menthe, glace",
                    Description = "à consommer à tout heure"
                }
            );
            modelBuilder.Entity<SuperRecette>()
            .HasMany(e => e.Ingredients).WithMany(e => e.Recettes);
        }

        public DbSet<SuperRecette> SuperRecettes { get; set; } 

        public DbSet<Ingredient> Ingredients { get; set; }
    }
}
