﻿using CocktailClient.Server.Data;
using CocktailClient.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CocktailClient.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperRecetteController : ControllerBase
    {
        private readonly DataContext _context;

        public SuperRecetteController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<SuperRecette>>> GetSuperRecettes()
        {
            var recettes = await _context.SuperRecettes.Include(sr => sr.Ingredients).ToListAsync();
            return Ok(recettes);
        }

        [HttpGet("ingredients")]
        public async Task<ActionResult<List<Ingredient>>> GetIngredients()
        {
            var ingredients = await _context.Ingredients.ToListAsync();
            return Ok(ingredients);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SuperRecette>> GetSingleRecette(int id)
        {
            var recette = await _context.SuperRecettes
                .Include(r => r.Ingredients)
                .FirstOrDefaultAsync(r => r.Id == id);
            if (recette == null)
            {
                return NotFound("Sorry, no recette here. :/");
            }
            return Ok(recette);
        }

        [HttpPost]
        public async Task<ActionResult<List<SuperRecette>>> CreateSuperRecette(SuperRecette recette)
        {
            recette.Ingredients = null;
            _context.SuperRecettes.Add(recette);
            await _context.SaveChangesAsync();

            return Ok(await GetDbRecettes());
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<List<SuperRecette>>> UpdateSuperRecette(SuperRecette recette, int id)
        {
            var dbRecette = await _context.SuperRecettes
                .Include(sr => sr.Ingredients)
                .FirstOrDefaultAsync(sr => sr.Id == id);
            if (dbRecette == null)
                return NotFound("Sorry, but no recette for you. :/");

            dbRecette.Name = recette.Name;
            string ingredientName = recette.IngredientName;
            dbRecette.IngredientName = ingredientName;
            dbRecette.Description = recette.Description;

            await _context.SaveChangesAsync();

            return Ok(await GetDbRecettes());
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<List<SuperRecette>>> DeleteSuperRecette(int id)
        {
            var dbRecette = await _context.SuperRecettes
                .Include(sr => sr.Ingredients)
                .FirstOrDefaultAsync(r => r.Id == id);
            if (dbRecette == null)
                return NotFound("Sorry, but no recette for you. :/");

            _context.SuperRecettes.Remove(dbRecette);
            await _context.SaveChangesAsync();

            return Ok(await GetDbRecettes());
        }

        private async Task<List<SuperRecette>> GetDbRecettes()
        {
            return await _context.SuperRecettes.Include(sr => sr.Ingredients).ToListAsync();
        }
    }
}
