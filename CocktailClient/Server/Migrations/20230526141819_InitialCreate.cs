﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace CocktailClient.Server.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IngredientName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuperRecettes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    IngredientName = table.Column<string>(type: "text", nullable: false),
                    IngredientId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperRecettes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IngredientSuperRecette",
                columns: table => new
                {
                    IngredientsId = table.Column<int>(type: "integer", nullable: false),
                    RecettesId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngredientSuperRecette", x => new { x.IngredientsId, x.RecettesId });
                    table.ForeignKey(
                        name: "FK_IngredientSuperRecette_Ingredients_IngredientsId",
                        column: x => x.IngredientsId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IngredientSuperRecette_SuperRecettes_RecettesId",
                        column: x => x.RecettesId,
                        principalTable: "SuperRecettes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Ingredients",
                columns: new[] { "Id", "IngredientName" },
                values: new object[,]
                {
                    { 1, "rhum" },
                    { 2, "grenadine" },
                    { 3, "whisky" },
                    { 4, "glace" }
                });

            migrationBuilder.InsertData(
                table: "SuperRecettes",
                columns: new[] { "Id", "Description", "IngredientId", "IngredientName", "Name" },
                values: new object[,]
                {
                    { 1, "boisson forte", 0, "amaretto, cognac, bailey", "ABC" },
                    { 2, "à consommer à tout heure", 0, "sirop de grenadine, eau", "Grenadine" },
                    { 3, "à consommer à tout heure", 0, "rhum, sucre de canne, eau gazeuse, menthe, glace", "Mojito" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_IngredientSuperRecette_RecettesId",
                table: "IngredientSuperRecette",
                column: "RecettesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IngredientSuperRecette");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "SuperRecettes");
        }
    }
}
